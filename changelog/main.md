** CipherOS 2.5 Lynx **
- Android Security Patch :
 * 5th February 2022

[Misc]
* Bump to CipherOS 2.5 - Lynx

[Security]
* This Update Brings Feb 2022 Security Patch & fixes some known issues
* Merged Tag android-12.0.0_r29

[Optimization]
* We've optimized our source even more , reduced debugging & fixed some known bugs.

[System]
* Introduce Android 12.1/12L Style Internet & Screenrecord
* Add VoLTE Icon Toggle
* Implement SystemUI Tuner
* Introduce alarm and vibrate status bar icons
* Fix some issues on arm devices
* Fix Screenrecorder on some MediaTek Devices
* Introduce Always On Small Clock
* Fix Bootanimation on Some MediaTek PowerVR GPU Devices

[HomeScreen]
* Themed Leftover icons
* Implement colorkt-based theme engine for homescreen


------------------------------------------------------------------

** CipherOS 2.1 SHADE **
- Android Security Patch :
 * 5th January 2022

-System:
* Based on android-12.0.0_r26
* Redesigned Settings
* Introduced CipherWidget
* Introduced CipherOS GO Edition
* Added new Wallpapers
* Added Monet Settings
* Added Network Traffic Indicator
* Added VoLTE icon from Moto
* Added Advanced Reboot
* Added Exact Calculator for Vanilla builds
* Adapted every Vanilla AOSP app for themed icons
* Switched to GSans font
* Safetynet should pass by default
* Many more misc changes
